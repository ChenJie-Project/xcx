package com.wzy.xcx.model.master;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModelMasterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ModelMasterApplication.class, args);
    }
}
