package cog.support.email;

/**
 * 邮件发送服务
 *
 * @author 陈杰
 * @since 2016年12月9日 16:03:52
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public interface IEmailSendServices {

    /**
     * 配置默认参数
     *
     * @param parameter 参数名称
     * @param value  参数值
     * */
    IEmailSendServices configDefaultParameters(String parameter,Object value);

    /**
     * 设置参数
     *
     * @param parameter 参数名称
     * @param value  参数值
     * */
    IEmailSendServices setParameters(String parameter,Object value);

    /**
     * 创建同步邮件发送任务
     * */
    IEmailSendJob createEmailSynchJob();

    /**
     * 创建异步的邮件发送任务
     * */
    IEmailSendJob createEmailAsynchJob();
}
