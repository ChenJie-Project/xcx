package cog.support.email.content;

/**
 * 邮件内容接口
 *
 * @author 陈杰
 * @since 2016年12月9日 16:03:52
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public interface IEmailContent {
    /**
     * 获取邮件内容
     * */
    String getEmailContent();
}
