package cog.support.email;

/**
 * 邮件发送任务
 *
 * @author 陈杰
 * @since 2016年12月9日 16:03:52
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public interface IEmailSendJob {

    /**
     * 发送邮件
     * */
    void send() throws Exception;

    /**
     * 发送邮件，发回邮件发送进度，由于大批量的异步发送
     * */
    IEmailSendProgress send(String job);

    /**
     * 结束发送
     * */
    void end();
}
