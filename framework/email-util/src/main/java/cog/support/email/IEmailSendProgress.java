package cog.support.email;

/**
 * 邮件发送进度
 *
 * @author 陈杰
 * @since 2016年12月9日 16:03:52
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public interface IEmailSendProgress {

    /**
     * 获取总任务数量
     * */
    int getJobCount();

    /**
     * 获取一处理的任务数量
     * */
    int processedCount();

    /**
     * 获取处理失败的任务数量
     * */
    int errorCount();

    /**
     * 获取处理结果
     * */
    boolean processedResult();
}
