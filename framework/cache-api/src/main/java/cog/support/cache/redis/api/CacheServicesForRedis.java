package cog.support.cache.redis.api;

import cog.support.cache.api.ICacheServices;
import cog.support.cache.redis.JedisDataSource;
import cog.support.util.common.StringKit;
import cog.support.util.serializable.CogSerializable;
import cog.support.util.serializable.CogSerializableFactory;
import cog.support.util.serializable.IRedisSerializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Redis的缓存实现类
 *
 * @author 陈杰
 * @since 2017年2月4日 22:15:23
 * @version v0.1
 *
 * Copyright 陈杰(chenjie_java@aliyun.com)
 */
public class CacheServicesForRedis implements ICacheServices {
    private Logger logger = LoggerFactory.getLogger(CacheServicesForRedis.class);
    private CogSerializable<Map<String,String>> cogSerializable = CogSerializableFactory.getHashMapSerializable();

    private JedisDataSource jedisDataSource;

    public JedisDataSource getJedisDataSource() {
        return jedisDataSource;
    }

    public void setJedisDataSource(JedisDataSource jedisDataSource) {
        this.jedisDataSource = jedisDataSource;
    }

    /**
     * 转换Redis的数据
     * */
    public static <T extends IRedisSerializable> T parseToObject(Map<String, String> mapIn, T object){
        if(mapIn==null || mapIn.size()<=0 || object == null) return null;
        object.loadRedisData(mapIn);
        return object;
    }



    private <T> void cacheObject(String key,T object,Jedis jeids){
        if(object!=null) {
            Map<String, String> map = cogSerializable.serializableToObject(object);
            jeids.hmset(key, map);
        }
    }

    @Override
    public <T> void cacheObject(String key, T object) {
        Jedis jedis = null;
        try{
            jedis = jedisDataSource.getResource();
            this.cacheObject(key,object,jedis);
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
    }

    @Override
    public <T> void cacheObject(String keyArray, T... object) {
        Jedis jedis = null;
        try{
            if(StringKit.isNotEmpty(keyArray)) {
                String[] keys = keyArray.split(",");
                if (keys != null && object!=null && keys.length == object.length){
                    jedis = jedisDataSource.getResource();
                    for(int i = 0;i<=keys.length;i++){
                        String key = keys[i];
                        T obj = object[i];
                        this.cacheObject(key,obj,jedis);
                    }
                }
                return;
            }
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }

    }

    @Override
    public <T> void cacheObject(Map<String, T> inMap) {
        Jedis jedis = null;
        try{
            if(inMap!=null && inMap.size()>0) {
                jedis = jedisDataSource.getResource();
                for(String key : inMap.keySet()) this.cacheObject(key,inMap.get(key),jedis);
            }
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
    }

    private <T> T getObject(Jedis jedis,String key,Class<?> tClass) throws Exception {
        Map<String,String> map = jedis.hgetAll(key);
        T ob = cogSerializable.serializableFromObject(map,tClass);
        return ob;
    }

    private <T> List<T> getObjectList(Jedis jedis,Class<?> tClass,String... keys) throws Exception {
        T object = null;
        List<T> list = new ArrayList<>();
        for(String key : keys) {
            object = this.getObject(jedis, key,tClass);
            list.add(object);
        }
        return list;
    }

    @Override
    public <T> T getObject(String key,Class<?> tClass) {
        Jedis jedis = null;
        try{
            jedis = jedisDataSource.getResource();
            return this.getObject(jedis,key,tClass);
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
        return null;
    }

    @Override
    public <T> List<T> getObjectList(String keyArrays,Class<?> tClass) {
        Jedis jedis = null;
        try {
            if (StringKit.isNotEmpty(keyArrays)){
                jedis = jedisDataSource.getResource();
                return this.getObjectList(jedis, tClass, keyArrays.split(","));
            }
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
        return null;
    }

    @Override
    public <T> List<T> getObjectList(Class<?> tClass,String... keys) {
        Jedis jedis = null;
        try{
            if(!(keys!=null && keys.length>0)) {
                jedis = jedisDataSource.getResource();
                return this.getObjectList(jedis, tClass, keys);
            }
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
        return null;
    }

    private void removeObject(Jedis jedis,String... keys){
        jedis.del(keys);
    }

    @Override
    public void removeObject(String key) {
        String[] keys = new String[1];
        keys[0] = key;
        this.removeObject(keys);
    }

    @Override
    public void removeObject(String... keys) {
        Jedis jedis = null;
        try{
            jedis = jedisDataSource.getResource();
            this.removeObject(jedis,keys);
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
    }

    @Override
    public <T> void cacheList(String key, List<T> inList) {
        Jedis jedis = null;
        try{
            jedis = jedisDataSource.getResource();
            for(T object : inList){
                String keyuuid = StringKit.getUUId();
                this.cacheObject(keyuuid,object,jedis);
                jedis.lpush(key,keyuuid);
            }
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
    }

    /**
     * 此处引用了lset方法，当index大于列表key的长度时，会报错
     * @param key 索引
     * @param index 指定位置
     * @param object 值
     */
    @Override
    public <T> void cacheList(String key, int index, T object) {
        Jedis jedis = null;
        try{
            jedis = jedisDataSource.getResource();
            String keyuuid = StringKit.getUUId();
            this.cacheObject(keyuuid,object,jedis);
            int i = new Long(jedis.llen(key)).intValue();
            if(i>index) jedis.lset(key,index,keyuuid);
            else jedis.lset(key,i,keyuuid);
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
    }

    private <T> List<T> getList(Jedis jedis,String key, int start, int size,Class<?> tClass) throws Exception {
        List<String> list = jedis.lrange(key,start,size);
        String[] strArray = new String[list.size()];
        list.toArray(strArray);
        return this.getObjectList(jedis,tClass,strArray);
    }

    @Override
    public <T> List<T> getList(String key,Class<?> tClass) {
        Jedis jedis =null;
        try{
            jedis = jedisDataSource.getResource();
            if(StringKit.isNotEmpty(key))
                return this.getList(jedis,key,0,-1,tClass);
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
        return null;
    }

    @Override
    public <T> List<T> getList(String key, int start, int size,Class<?> tClass) {
        Jedis jedis = null;
        try{
            jedis = jedisDataSource.getResource();
            return this.getList(jedis,key,start,size,tClass);
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
        return null;
    }

    @Override
    public <T> T getListObject(String key, int index,Class<?> tClass) {
        Jedis jedis = null;
        try{
            jedis = jedisDataSource.getResource();
            List<T> list = this.getList(jedis,key,index,0,tClass);
            return list.get(0);
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }
        return null;
    }

    @Override
    public void removeList(String key) {
        String[] keys = new String[1];
        keys[0] = key;
        this.removeObject(keys);

    }

    @Override
    public void removeList(String key, int index) {
        Jedis jedis = null;
        String keyuuid = jedis.lindex(key,index);
        String[] keys = new String[1];
        keys[0] = keyuuid;
        this.removeObject(keys);
    }

    @Override
    public void removeList(String key, int start, int size) {
        Jedis jedis = null;
        String[] keys = new String[size];
        List<String> keyuuids = jedis.lrange(key,start,size);
        String[] strArray = new String[keyuuids.size()];
        keyuuids.toArray(strArray);
        this.removeObject(strArray);
    }

    @Override
    public void clear(String tag) {
        Jedis jedis = null;
        try{
            jedis = jedisDataSource.getResource();
        }catch (Exception ex){
            logger.error("{}",ex);
        }finally {
            jedisDataSource.returnResource(jedis);
        }

    }
}
