package cog.support.cache.ehcache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Ehcache
 *
 * @author 陈杰
 * @since 2016年9月12日 00:53:51
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com) 2016年9月12日 00:53:51
 */
public class EhcacheManager {
    private static final Logger logger = LoggerFactory.getLogger(EhcacheManager.class);

    private static final String DEFAULT_CACHE = "cache";

    private CacheManager cacheManager;

    public EhcacheManager(){
        this.cacheManager  = CacheManager.getInstance();
    }

    /**
     * 缓存对象
     *
     * @param key 建
     * @param object 对象
     * */
    public Cache putObject(String key,Object object){
        Cache cache = this.getCache();
        this.putObject(cache,key,object);
        return cache;
    }

    /**
     * 缓存对象
     *
     * @param keyArrays 多个建，用“，”分割
     * @param object 多个对象
     * */
    public Cache putObject(String keyArrays,Object... object){
        Cache cache = this.getCache();
        this.putObject(cache,keyArrays,object);
        return cache;
    }

    /**
     * 缓存对象
     *
     * @param cache Ecache缓存对象
     * @param key 建
     * @param object 对象
     * */
    public void putObject(Cache cache,String key,Object object){
        cache.put( new Element(key,object));
    }

    /**
     * 缓存对象
     *
     * @param cache Ecache缓存对象
     * @param keyArrays 多个建，用“，”分割
     * @param object 多个对象
     * */
    public void putObject(Cache cache,String keyArrays,Object... object){
        String[] keys = keyArrays.split(",");
        for(int i = 0;i<= keys.length;i++)
            cache.put(new Element(keys[i], object[i]));
    }

    /**
     * 通过KEY获取缓存对象
     *
     * @param key 缓存key
     * */
    public Object getObjectByKey(Cache cache,String key){
        Element element = cache.get(key);
        return element.getObjectValue();
    }

    /**
     * 通过KEY获取缓存Java对象
     *
     * @param key 缓存key
     * */
    public <E> E getJavaObjectByKey(Cache cache,String key){
        return (E) this.getObjectByKey(cache,key);
    }

    /**
     * 获取缓存操作对象
     * */
    public Cache getCache(){
        return this.cacheManager.getCache(DEFAULT_CACHE);
    }

    /**
     * 通过配置ID获取缓存操作对象
     * */
    public Cache getCache(String cacheConfig){
        return this.cacheManager.getCache(cacheConfig);
    }
}
