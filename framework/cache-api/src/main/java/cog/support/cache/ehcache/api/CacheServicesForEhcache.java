package cog.support.cache.ehcache.api;

import cog.support.cache.api.ICacheServices;
import cog.support.cache.ehcache.EhcacheManager;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * Ehcache的缓存实现类
 *
 * @author 王晨
 * @since 2016年11月21日
 * @version v0.1
 *
 * Copyright 王晨(2457415798@qq.com)
 */
public class CacheServicesForEhcache implements ICacheServices {
    private Logger logger = LoggerFactory.getLogger(CacheServicesForEhcache.class);


    @Autowired
    private EhcacheManager ehcacheManager;

    /**
     * 缓存  缓存一条(Key,value)-->(String,T)的数据
     *
     * @param key 索引
     * @param object 值
     */
    @Override
    public <T> void cacheObject(String key, T object) {
        Cache cache = ehcacheManager.getCache();
        cache.put(new Element(key,object));
    }

    @Override
    public <T> void cacheObject(String keyArray, T... object) {
        Cache cache = ehcacheManager.getCache();
        cache.put(new Element(keyArray,object));
    }

    @Override
    public <T> void cacheObject(Map<String, T> inMap) {
        Cache cache = ehcacheManager.getCache();
        Set<Map.Entry<String,T>> entries= inMap.entrySet();
        Iterator iterator = entries.iterator();
        while(iterator.hasNext()){
            Map.Entry mapentry = (Map.Entry)iterator.next();
            cache.put(new Element(mapentry.getKey(),mapentry.getValue()));
        }
    }

    @Override
    public <T> T getObject(String key,Class<?> tClass) {
        Cache cache = ehcacheManager.getCache();
        Element e = cache.get(key);
        return (T)e.getObjectValue();
    }

    @Override
    public <T> List<T> getObjectList(String keyArrays,Class<?> tClass) {
        List<T> list = new ArrayList<>();
        Cache cache = ehcacheManager.getCache();
        String[] keys = keyArrays.split(",");
        for(String key : keys){
            Element e = cache.get(key);
            list.add((T)e);
        }
        return list;
    }

    @Override
    public <T> List<T> getObjectList(Class<?> tClass,String... keys) {
        List<T> list = new ArrayList<>();
        Cache cache = ehcacheManager.getCache();
        for(String key : keys){
            Element e = cache.get(key);
            if(e==null) continue;
            else list.add((T)e.getObjectValue());
        }
        return list;
    }

    /**
     * 希望可以改void为int或者boolean
     * @param key 索引
     */
    @Override
    public void removeObject(String key) {
        Cache cache = ehcacheManager.getCache();
        cache.remove(key);
    }

    @Override
    public void removeObject(String... keys) {
        Cache cache = ehcacheManager.getCache();
        cache.remove(keys);
    }

    @Override
    public <T> void cacheList(String key, List<T> inList) {
        Cache cache = ehcacheManager.getCache();
        cache.replace(new Element(key,inList));
    }

    @Override
    public <T> void cacheList(String key, int index, T object) {
        Cache cache = ehcacheManager.getCache();
        List<Object> list = (List<Object>) cache.get(key).getObjectValue();
        if(list == null ) list = new ArrayList<>();
        if(index>0)list.add(index,object);
        else list.add(object);
        cache.replace(new Element(key,list));
    }

    @Override
    public <T> List<T> getList(String key,Class<?> tClass) {
        Cache cache = ehcacheManager.getCache();
        Element e = cache.get(key);
        List<T> list = (List<T>) e.getObjectValue();
        return list;
    }

    @Override
    public <T> List<T> getList(String key, int start, int size,Class<?> tClass) {
        Cache cache = ehcacheManager.getCache();
        List<T> list = (List<T>) cache.get(key).getObjectValue();
        List<T> list1 = new ArrayList<>();
        if(size>0 && start>=0){
            for(int i =start;i<size+start;i++){
                list1.add(i,list.get(i));
            }
        }
        return list1;
    }

    @Override
    public <T> T getListObject(String key, int index,Class<?> tClass) {
        Cache cache = ehcacheManager.getCache();
        List<T> list = (List<T>) cache.get(key).getObjectValue();
        return list.get(index);
    }

    @Override
    public void removeList(String key) {
        Cache cache = ehcacheManager.getCache();
        cache.remove(key);

    }

    @Override
    public void removeList(String key, int index) {
        Cache cache = ehcacheManager.getCache();
        List<Object> list = (List<Object>) cache.get(key).getObjectValue();
        list.remove(index);
    }

    @Override
    public void removeList(String key, int start, int size) {
        Cache cache = ehcacheManager.getCache();
        List<Object> list = (List<Object>) cache.get(key).getObjectValue();
        for(int i =start;i<size+start;i++){
            list.remove(start);
        }
    }

    @Override
    public void clear(String tag) {
        Cache cache = ehcacheManager.getCache();
        cache.removeAll();

    }
}
