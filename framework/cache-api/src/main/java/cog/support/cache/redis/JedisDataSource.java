package cog.support.cache.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Jedis数据源
 * @author 陈杰
 * @since 2016年9月12日 00:53:51
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com) 2016年9月12日 00:53:51
 */
public class JedisDataSource{

    private static final Logger logger = LoggerFactory.getLogger(JedisDataSource.class);
    
    private JedisPool jedisPool;

    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }


    /**
     * 获取Jedis客户端
     * @return Jedis
     * */
    public Jedis getResource() {
        return jedisPool.getResource();
    }

    /**
     * 释放Jedis客户端
     * @param jedis
     * */
    public void returnResource(Jedis jedis){
        if(jedis!=null)  jedis.close();
    }

}