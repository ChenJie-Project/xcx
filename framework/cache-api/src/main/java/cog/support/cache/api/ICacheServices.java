package cog.support.cache.api;

import java.util.List;
import java.util.Map;

/**
 * 缓存接口
 *
 * @author 陈杰
 * @since 2016年9月12日 00:53:51
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com) 2016年9月12日 00:53:51
 */
public interface ICacheServices {
    /**
     * 缓存  缓存一条(Key,value)-->(String,T)的数据
     *
     * @param key 索引
     * @param object 值
     */
    <T> void cacheObject(String key,T object);

    /**
     * 缓存  缓存多条数据，格式为key="key1,key2,key3...",value=...object
     *
     * @param keyArray 索引
     * @param object 值
     */
    <T> void cacheObject(String keyArray,T ... object);

    /**
     * 缓存  缓存多条数据，格式为(Map(String,T)
     *
     * @param inMap Map(String,T)
     */
    <T> void cacheObject(Map<String,T> inMap);


    /**
     * 获取  通过key,获取一条对应的数据，
     *
     * @param key 索引
     * @return 返回一个值
     */
    <T> T getObject(String key,Class<?> tClass);

    /**
     * 获取  通过key="key1,key2,key3...",获取一个多条数据的List集合
     *
     * @param keyArrays 索引
     * @return 返回一个List集合，包含多个值
     */
    <T> List<T> getObjectList(String keyArrays,Class<?> tClass);

    /**
     * 获取  通过一组key，获取对应的一组数据的List集合
     *
     * @param keys 索引
     * @return 返回一个List集合，包含多个值
     */
    <T> List<T> getObjectList(Class<?> tClass,String ... keys);


    /**
     * 删除  通过key，删除对应的一条或多条数据缓存
     *
     * @param key 索引
     */
    void removeObject(String key);


    /**
     * 删除  通过一组key，删除对应的一组数据缓存
     *
     * @param keys 索引
     */
    void removeObject(String ... keys);


    /**
     * 缓存  缓存多条数据，格式为(key,List<T>)
     *
     * @param key 索引
     * @param inList 值
     */
    <T> void cacheList(String key,List<T> inList);

    /**
     * 缓存  缓存一条数据在index位置
     *
     * @param key 索引
     * @param index 指定位置
     * @param object 值
     */
    <T> void cacheList(String key,int index,T object);


    /**
     * 获取  通过key，获取一条List数据，
     *
     * @param key 索引
     * @return 返回一个List集合，包含多个值
     */
    <T> List<T> getList(String key,Class<?> tClass);

    /**
     * 获取  通过key和start(包括start)与size，获取一条从start位置开始，长度为size的List数据
     *
     * @param key 索引
     * @param start 起始位置
     * @param size 长度
     * @return 返回一个List集合，包含多个值
     */
    <T> List<T> getList(String key,int start,int size,Class<?> tClass);

    /**
     * 获取  通过key和index，从存入的List的index位置获取一条数据
     *
     * @param key 索引
     * @param index 指定位置
     * @return 返回一个数据
     */
    <T> T getListObject(String key,int index,Class<?> tClass);


    /**
     * 删除  通过key，删除一个List集合的数据
     *
     * @param key 索引
     */
    void removeList(String key);

    /**
     * 删除  通过key和index，删除一条在index位置的List集合
     *
     * @param key 索引
     * @param index 指定位置
     */
    void removeList(String key,int index);

    /**
     * 删除  通过key和start(包括start)与size，删除一条从start位置开始，长度为size的List数据
     *
     * @param key 索引
     * @param start 起始位置
     * @param size 长度
     */
    void removeList(String key,int start,int size);

    /**
     * 清空  清空缓存
     * @param tag 索引
     */
    void clear(String tag);
}
