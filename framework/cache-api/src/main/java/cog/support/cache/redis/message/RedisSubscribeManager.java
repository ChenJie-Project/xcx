package cog.support.cache.redis.message;

import cog.support.cache.redis.JedisDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;

import java.util.List;

/**
 * Jedis发布和订阅管理器
 *
 * @author 陈杰
 * @since 2016年9月12日 00:53:51
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class RedisSubscribeManager {
    private Logger logger = LoggerFactory.getLogger(RedisSubscribeManager.class);

    /**
     * Jedis 访问
     * */
    @Autowired
    private JedisDataSource jedisDataSource;

    /**
     * 消息处理器列表
     * */
    private List<RedisSubscribeMessageHandle> channelList;

    /**
     * 加载话题订阅
     * */
    public void loadChannel() {
        if (channelList != null && channelList.size() > 0)
            for (RedisSubscribeMessageHandle redisSubscribeMessageHandle : channelList) {
                (new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(true){
                            Jedis jedis = null;
                            try {
                                jedis = jedisDataSource.getResource();
                                String[] channel = redisSubscribeMessageHandle.getSubscribeChannle();
                                for(String chan : channel)
                                    logger.info("<消息管理器> 订阅通道器，通道：{}", chan);
                                jedis.subscribe(redisSubscribeMessageHandle, channel);
                                for(String chan : channel)
                                    logger.info("<消息管理器> 取消訂閲，通道：{}", chan);
                            } catch (Exception ex) {
                                logger.info("平道订阅消息出现异常,当前订阅的平道{},异常信息{}", redisSubscribeMessageHandle.getSubscribeChannle(),ex);
                            } finally {
                                jedisDataSource.returnResource(jedis);
                            }
                        }
                    }
                },"Redis-Linsner-Server("+ redisSubscribeMessageHandle.getHandleName()+")")).start();
            }
    }

    public JedisDataSource getJedisDataSource() {
        return jedisDataSource;
    }

    public void setJedisDataSource(JedisDataSource jedisDataSource) {
        this.jedisDataSource = jedisDataSource;
    }

    public List<RedisSubscribeMessageHandle> getChannelList() {
        return channelList;
    }

    public void setChannelList(List<RedisSubscribeMessageHandle> channelList) {
        this.channelList = channelList;
    }

}