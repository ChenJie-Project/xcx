package cog.support.cache.redis.message;

import cog.support.util.common.StringKit;
import redis.clients.jedis.JedisPubSub;

/**
 * Jedis发布和订阅封装
 *
 * @author 陈杰
 * @since 2016年9月12日 00:53:51
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public abstract class RedisSubscribeMessageHandle extends JedisPubSub {

    /**
     * 处理器频道
     * */
    private String[] channel;

    public String[] getChannel() {
        return channel;
    }

    /**
     * 消息处理器名称
     * */
    private String handleName;

    public String getHandleName() {
        return handleName;
    }

    public void setHandleName(String handleName) {
        this.handleName = handleName;
    }

    /**
     * 设置处理器频道
     *
     * @param channel ","相隔的多个频道
     * */
    public void setChannel(String channel) {
        if(StringKit.isNotEmpty(channel)) this.channel = channel.split("\\,");
    }

    public String[] getSubscribeChannle(){
        return this.getChannel();
    }
}
