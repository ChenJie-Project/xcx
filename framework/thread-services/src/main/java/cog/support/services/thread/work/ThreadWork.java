package cog.support.services.thread.work;

import cog.support.services.thread.IThreadService;
import cog.support.util.common.StringKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  工作线程抽象封装
 *
 * @author 陈杰
 * @since 2017年1月8日 16:59:41
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class ThreadWork extends Thread{
    private Logger logger = LoggerFactory.getLogger(ThreadWork.class);

    private int runIndex;

    /**
     * 处理失败次数
     * */
    private int error;

    /**
     * 运行唯一标识，32位uuid
     */
    private String runerId;

    /**
     * 启动标识
     */
    private boolean runMark;

    /**
     * 启动时间
     * */
    private long startTime;

    /**
     * 上次执行时间
     * */
    private long lastRunTime;

    /**
     * 执行工作
     * */
    private IThreadService threadService;


    public ThreadWork(int workIndex,String servicesName,IThreadService threadService,ThreadGroup threadGroup){
        super(threadGroup,servicesName+"-"+workIndex);
        this.runIndex = workIndex;
        this.runMark = true;
        this.threadService = threadService;
        this.runerId = StringKit.getUUId();
    }

    public ThreadWork(String servicesName,IThreadService threadService){
        super(servicesName);
        this.runIndex = 0;
        this.runMark = true;
        this.threadService = threadService;
        this.runerId = StringKit.getUUId();
    }

    public ThreadWork(String servicesName){
        super(servicesName);
        this.runIndex = 0;
        this.runMark = true;
        this.runerId = StringKit.getUUId();
    }

    public void run(){
        this.startTime = System.currentTimeMillis();
        Thread.currentThread().setName(Thread.currentThread().getName() + "-" + this.threadService.workName());
        logger.info("<后台服务> 名称:{}({}),开始启动运行!",this.threadService.workName(),this.runerId);
        while(this.runMark){
            try{
                this.threadService.work();
            }catch (Exception ex){
                if(++this.error >= Integer.MAX_VALUE) this.error = 0;
                this.exceptionHandle(ex);
            }finally {
                this.lastRunTime = System.currentTimeMillis();
            }
        }
        logger.info("<后台服务> 名称:{}({}),停止运行!",this.threadService.workName(),this.runerId);
    }

    /**
     * 异常处理
     * */
    public void exceptionHandle(Exception ex){
        logger.error("<后台服务> 名称:{}({}),处理发送异常异常信息{}!",this.threadService.workName(),this.runerId,ex.getMessage(),ex);
    }


    /**
     * 运行线程
     * */
    public void threadStart(){
        this.runMark = true;
        this.start();
    }

    /**
     * 停止线程
     * */
    public void threadStop(){
        this.runMark = false;
    }

    public int getRunIndex() {
        return runIndex;
    }

    public void setRunIndex(int runIndex) {
        this.runIndex = runIndex;
    }

    public IThreadService getThreadService() {
        return threadService;
    }

    public void setThreadService(IThreadService threadService) {
        this.threadService = threadService;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getRunerId() {
        return runerId;
    }

    public void setRunerId(String runerId) {
        this.runerId = runerId;
    }

    public boolean isRunMark() {
        return runMark;
    }

    public void setRunMark(boolean runMark) {
        this.runMark = runMark;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getLastRunTime() {
        return lastRunTime;
    }

    public void setLastRunTime(long lastRunTime) {
        this.lastRunTime = lastRunTime;
    }
}
