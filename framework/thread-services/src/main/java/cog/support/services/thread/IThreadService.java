package cog.support.services.thread;

/**
 * 服务线程，用于创建长期后台运行的线程
 *
 * @author 陈杰
 * @since 2017年1月8日 16:59:41
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public interface IThreadService {

    /**
     * 具体的工作内容
     * */
    void work() throws Exception;

    /**
     * 获取工作名称
     * */
    String workName();

    /**
     * 设置工作名称
     * */
    void workName(String name);

    /**
     * 获取当前服务最大运行线程数量
     * */
    int maxRunCount();

    /**
     * 设置当前服务最大运行线程数量
     *
     * @param count 运行线程数量
     * */
    void maxRunCount(int count);

}
