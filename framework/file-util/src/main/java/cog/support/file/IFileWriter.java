package cog.support.file;

import cog.support.file.adapter.IFileDataAdapter;

import java.io.OutputStream;

/**
 * 文件写入
 *
 * @author 陈杰
 * @time 2016年4月14日 14:48:53
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public interface IFileWriter {

    /**
     * 打开或者创建一个File
     * */
    void openOrCreateFile(String path);

    /**
     * 写入文件
     *
     * @param dataAdapter 文件数据适配器
     * */
    void writeFile(IFileDataAdapter dataAdapter);

    /**
     * 保存并且关闭文件流
     * */
    void saveAndClose();

    /**
     * 导出文件流
     *
     * @return OUtputStream
     * */
    OutputStream outStream();

}
