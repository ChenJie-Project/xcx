package cog.support.file;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * 文件数据行对象，一行对应一条记录
 *
 * @author 陈杰
 * @time 2016年8月25日 19:40:32
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class FileData {
    private int rownum;
    private String rowid;
    private List<String> rowValue;

    public FileData(int rownum,String rowid){
        this.rownum = rownum;
        this.rowid = rowid;
        this.rowValue = new LinkedList<String>();
    }

    public FileData(int rownum){
        this.rownum = rownum;
        this.rowid = (UUID.randomUUID()).toString();
        this.rowValue = new LinkedList<String>();
    }

    public FileData(){
        this.rownum = 0;
        this.rowid = (UUID.randomUUID()).toString();
        this.rowValue = new LinkedList<String>();
    }

    public int getRownum() {
        return rownum;
    }

    public void setRownum(int rownum) {
        this.rownum = rownum;
    }

    public String getRowid() {
        return rowid;
    }

    public void setRowid(String rowid) {
        this.rowid = rowid;
    }

    public List<String> getRowValue() {
        return rowValue;
    }

    public void setRowValue(List<String> rowValue) {
        this.rowValue = rowValue;
    }

    public void addRowValue(int cellIndex,String value) {
        this.rowValue.add(cellIndex,value);
    }

    public void addRowValue(String value) {
        this.rowValue.add(value);
    }

}
