package cog.support.file;

import cog.support.file.adapter.IFileDataAdapter;

import java.io.InputStream;

/**
 * 文件写入
 *
 * @author 陈杰
 * @time 2016年4月14日 14:48:53
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public interface IFileReader {
    /**
     * 写入文件
     *
     * @param filePath 文件路径
     * */
    IFileDataAdapter readFromFile(String filePath);

    /**
     * 写入文件
     *
     * @param is 文件输入流
     * */
    IFileDataAdapter readFromFile(InputStream is);
}
