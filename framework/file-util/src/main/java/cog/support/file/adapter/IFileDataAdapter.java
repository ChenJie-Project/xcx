package cog.support.file.adapter;

import cog.support.file.FileData;
import cog.support.file.FileDataHead;

import java.util.List;

/**
 * 文件数据转换
 *
 * @author 陈杰
 * @time 2016年8月25日 18:46:37
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public interface IFileDataAdapter<T> {

    /**
     * 加载数据,为写入初始化
     *
     * @[param data
     * */
    void loadForWrite(T data);

    /**
     * 加载数据,为写入初始化
     *
     * @[param dataList
     * */
    void loadForWrite(List<? extends T> dataList);

    /**
     * 加载数据,为读取初始化
     *
     * @[param data
     * */
    void loadForRead(FileDataHead fileDataHead,FileData fileData);

    /**
     * 逐条获取文件数据对象
     *
     * @return FileData
     * */
    FileData next();

    /**
     * 获取文件头部信息
     *
     * @return FileDataHead
     * */
    FileDataHead createHead();

    /**
     * 读取一条文件记录
     * */
    T read(FileData fileData);

}
