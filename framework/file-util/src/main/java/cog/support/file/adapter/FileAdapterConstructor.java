package cog.support.file.adapter;

import java.util.List;
import java.util.Map;

/**
 * 文件数据对象构造器
 *
 * @author 陈杰
 * @time 2016年8月25日 19:06:06
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class FileAdapterConstructor {

    /**
     * 文件数据对象构造器，单列模式
     * */
    private FileAdapterConstructor(){this.init();}

    private static class FileAdapterConstructorProxy{
        public static final FileAdapterConstructor instance = new FileAdapterConstructor();
    }

    /**
     * 文件数据对象构造器，单列模式
     * 获取一个单列
     *
     * @return FileAdapterConstructor
     * */
    public static FileAdapterConstructor getInstance(){
        return FileAdapterConstructorProxy.instance;
    }

    /**
     * 初始化方法
     * */
    private void init() {
    }


    /**
     * 创建Java Bean格式文件数据适配器
     *
     * @param clazs
     * */
    public <T> IFileDataAdapter<T> createFileDataAdapterFromClass(Class<? extends T> clazs){
        return null;
    }
    /**
     * 创建Java Bean格式文件数据适配器,多行模式
     *
     * @param list
     * */
    public <T> IFileDataAdapter createFileDataAdapterFromClass(List<T> list){
        return null;
    }


    /**
     * 创建String格式文件数据适配器
     *
     * @param json
     * */
    public IFileDataAdapter<String> createFileDataAdapterFromJson(String json){
        return null;
    }
    /**
     * 创建String格式文件数据适配器,多行模式
     *
     * @param jsonList
     * */
    public IFileDataAdapter<String> createFileDataAdapterFromJson(List<String> jsonList){
        return null;
    }


    /**
     * 创建Map格式文件数据适配器
     *
     * @param mapData
     * */
    public IFileDataAdapter<Map<String,Object>> createFileDataAdapterFromMap(Map<String,Object> mapData){
        return null;
    }
    /**
     * 创建Map格式文件数据适配器,多行模式
     *
     * @param mapDataList
     * */
    public IFileDataAdapter<List<Map<String,Object>>> createFileDataAdapterFromMap(List<Map<String,Object>> mapDataList){
        return null;
    }

    /**
     * 创建文件数据读取适配器
     *
     * @param clazs
     * */
    public <T> IFileDataAdapter<T> createFileDataAdapterForRead(Class<? extends T> clazs){
        return null;
    }

    /**
     * 创建文件数据读取适配器,多行模式
     *
     * @param clazs
     * */
    public <T> IFileDataAdapter<List<T>> createListFileDataAdapterForRead(Class<? extends T> clazs){
        return null;
    }

}
