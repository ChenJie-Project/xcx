package cog.support.file;

import java.util.LinkedList;
import java.util.List;

/**
 * 文件数据行对象，一个文件对应一个对象
 *
 * @author 陈杰
 * @time 2016年8月25日 19:40:32
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class FileDataHead {
    /**
     * 总行数
     * */
    private int rowCount;

    /**
     * 总列数
     * */
    private int cellCount;

    /**
     * 文件名称，可空
     * */
    private String fileName;

    /**
     * 自定义名称，可以空
     * */
    private String name;

    /**
     * 行数据对象
     * */
    private List<FileData> dataList;


    public FileDataHead(int rowCount,int cellCount){
        this.rowCount = rowCount;
        this.cellCount = cellCount;
        this.dataList = new LinkedList<FileData>();
    }

    public FileDataHead(){
        this.dataList = new LinkedList<FileData>();
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    public int getCellCount() {
        return cellCount;
    }

    public void setCellCount(int cellCount) {
        this.cellCount = cellCount;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FileData> getDataList() {
        return dataList;
    }

    public FileData getDataList(int rowIndex) {
        return dataList.get(rowIndex);
    }

    public void setDataList(List<FileData> dataList) {
        this.dataList = dataList;
    }

    public void addDataList(FileData data) {
        this.dataList.add(data);
    }

    public void addDataList(int rowIndex,FileData data) {
        this.dataList.add(rowIndex,data);
    }
}
