package cog.support.file;

/**
 * 文件写入创建工厂
 *
 * @author 陈杰
 * @time 2016年4月14日 14:48:53
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class FileWriteFactory {


    /**
     * 文件写入创建工厂，单列模式
     * */
    private FileWriteFactory(){this.init();}

    private static class FileWriteFactoryProxy{
        public static final FileWriteFactory instance = new FileWriteFactory();
    }

    /**
     * 文件写入创建工厂，单列模式
     * 获取一个单列
     *
     * @return FileAdapterConstructor
     * */
    public static FileWriteFactory getInstance(){
        return FileWriteFactoryProxy.instance;
    }

    /**
     * 初始化方法
     * */
    private void init() {
    }


    /**
     * 根据文件后缀，创建文件写入对象
     * */
    public IFileWriter createFileWriterBySuffix(String suffix){
        IFileWriter fileWriter = null;
        if(suffix.toLowerCase().indexOf(".txt")>0){

        } else if(suffix.toLowerCase().indexOf(".pdf")>0){

        } else if(suffix.toLowerCase().indexOf(".xlsx")>0){

        } else if(suffix.toLowerCase().indexOf(".xls")>0){

        } else if(suffix.toLowerCase().indexOf(".docx")>0){

        } else if(suffix.toLowerCase().indexOf(".doc")>0) {

        }
        return fileWriter;
    }

}
