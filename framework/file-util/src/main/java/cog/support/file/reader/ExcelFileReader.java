package cog.support.file.reader;

import cog.support.file.IFileReader;
import cog.support.file.adapter.IFileDataAdapter;

import java.io.InputStream;

/**
 * 读取文件
 *
 * @author 陈杰
 * @time 2016年8月25日 19:06:06
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class ExcelFileReader<T> implements IFileReader {
    private IFileDataAdapter<T> fileDataAdapter;

    public ExcelFileReader(IFileDataAdapter<T> fileDataAdapter){
        this.fileDataAdapter = fileDataAdapter;
    }


    @Override
    public IFileDataAdapter readFromFile(String filePath) {
        return null;
    }

    @Override
    public IFileDataAdapter readFromFile(InputStream is) {
        return null;
    }

}
