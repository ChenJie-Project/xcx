package cog.support.springmvc.hashmap;

import cog.support.springmvc.session.ISession;
import cog.support.util.common.StringKit;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 使用HashMap实现的Session
 *
 * @author 陈杰
 * @time 2016年9月19日 20:00:24
 * @version v0.1
 *
 * Copyright DongHui(chenjie_java@aliyun.com)
 */
public class HashMapSession implements ISession {

    private String sessionId;

    private Date createDate;

    private Map<String,Object> objectMap;

    public HashMapSession(){
        this.sessionId = StringKit.getUUId();
        this.createDate = new Date();
        this.objectMap = new HashMap<>();
    }

    @Override
    public void addParameter(String key, Object value) {
        this.objectMap.put(key,value);
    }

    @Override
    public <T> T getParameter(String key) {
        return (T) this.objectMap.get(key);
    }

    @Override
    public void removeParameter(String key) {
        this.objectMap.remove(key);
    }

    @Override
    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId){
        this.sessionId = sessionId;
    }

    @Override
    public Date getCreateTime() {
        return this.createDate;
    }
}
