package cog.support.springmvc.session;

/**
 * 用户Seesion池接口
 * 
 * @author 陈杰
 * @time 2016年9月12日 03:35:34
 * @version v0.1
 * 
 * Copyright DongHui(chenjie_java@aliyun.com)
 */
public interface ISessionPool {
	/**
	 * 根据tokenId获取Session
	 * @param tokenId
	 * @reutrn ISession
	 * */
	ISession getSession(String tokenId);
	
	/**
	 * 根据tokenId获取并移除Session
	 * @param tokenId
	 * @reutrn ISession
	 * */
	ISession pollSession(String tokenId);

	/**
	 * 创建并保存Session
	 * 
	 * @param key key集合，用“,”分割
	 * @param value value集合
	 * @return tokenId
	 * */
    String createSession(String key, Object... value);

    /**
     * 创建并保存Session
     *
     * @return tokenId
     * */
    ISession createSession();

	/**
	 * 创建并或者返回Session
	 *
	 * @return tokenId
	 * */
	ISession getOrCreateSession(String tokenId);
}
