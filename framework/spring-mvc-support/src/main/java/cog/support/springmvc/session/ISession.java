package cog.support.springmvc.session;

import java.util.Date;

/**
 * 用户会话对象
 * 
 * @author 陈杰
 * @time 2016年9月12日 03:35:34
 * @version v0.1
 * 
 * Copyright DongHui(chenjie_java@aliyun.com)
 */
public interface ISession {
	/**
	 * 添加参数
	 *
	 * @param  key
	 * @param  value
	 * */
	void addParameter(String key, Object value);

	/**
	 * 获取参数
	 *
	 * @param  key
	 * */
	<T> T getParameter(String key);

	/**
	 * 移除参数
	 *
	 * @param  key
	 * */
	void removeParameter(String key);

	/**
	 * 获取Session
	 * */
	String getSessionId();

	/**
	 * 设置SessionId
	 * */
	void setSessionId(String sessionId);

	/**
	 * 获取创建时间
	 * */
	Date getCreateTime();


}
