package cog.support.springmvc;

import cog.support.util.common.StringKit;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 控制器基础类
 * 
 * @author 陈杰
 * @time 2016年4月14日 14:48:53
 * @version v0.1
 * 
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class BaseSpringController {
    /**
     * UserSession 保存Key
     */
    public static final String USER_SESSION_KEY = "UserSessionKey";


    public static final String USER_BACKGROUND_JOB_KEY = "UserBackGroundJobKey";


    public static final String USER_SEND_SESSION_TOCKEN = "UserSendSessionTocken";

    /**
     * 分页数据的key
     * */
    protected final String DATA_PAGE_KEY = "DataPage";

    /**
     * 根据tokenId获取UserSession
     * @return UserSession
     * */
    public HttpSession getSession(){
        return this.getReqeust().getSession();
    }

    /**
     * 获取请求对象
     *
     * @return HttpServletRequest 请求对象
     * */
    protected HttpServletRequest getReqeust(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return request;
    }

    /**
     * 获取响应对象
     *
     * @return
     * @author 胡娅婷
     */
    protected HttpServletResponse getResponse(){
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        return response;
    }

    public String getIpAddress(){
        try {
            HttpServletRequest request = this.getReqeust();
            String ip = request.getHeader("X-Forwarded-For");
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
                    ip = request.getHeader("Proxy-Client-IP");
                if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
                    ip = request.getHeader("WL-Proxy-Client-IP");
                if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
                    ip = request.getHeader("HTTP_CLIENT_IP");
                if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
                    ip = request.getHeader("HTTP_X_FORWARDED_FOR");
                if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
                    ip = request.getRemoteAddr();
            } else if (ip.length() > 15) {
                for (String strIp : ip.split(","))
                    if (!("unknown".equalsIgnoreCase(strIp))) return strIp;
            }
            return ip;
        }catch (Exception ex){
            return "0.0.0.0";
        }
    }

    /**
     * 获取请求对象
     *
     * @return HttpServletRequest 请求对象
     * */
    protected Map<String,Object> getRequestMap(){
        HttpServletRequest request = this.getReqeust();
        Map<String,Object> requestMap = new HashMap<>();
        Enumeration<String> keys = request.getParameterNames();
        while(keys.hasMoreElements()) {
            String k = keys.nextElement();
            requestMap.put(k,request.getParameter(k));
        }
        return requestMap;
    }




}