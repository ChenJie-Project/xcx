package cog.support.springmvc.model;



/**
 * 请求实体类
 * 
 * @author 陈杰
 * @time 2016年9月12日 03:35:34
 * @version v0.1
 * 
 * Copyright DongHui(chenjie_java@aliyun.com)
 */
public class BaseRequestModel{
	private String tokenId;

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
}
