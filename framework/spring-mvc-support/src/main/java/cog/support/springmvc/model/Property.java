package model;

import cog.support.util.common.StringKit;

/**
 * 属性key的基础操作
 * @author 王晨
 * @since 2016/9/28
 * @version 0.1
 *
 * Copyright Co-lander  (co_loander@163.com)
 */
public class Property {
    private String key;
    private String[] singleKey = new String[20];

    private void initParameter(String str){
        if(StringKit.isNotEmpty(str)){
            String[] param = str.split(",");
            for (int i=0;i<param.length;i++){
                this.singleKey[i] =param[i];
            }

        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;

        this.initParameter(key);
    }

    public String[] getSingleKey() {
        return singleKey;
    }

    public void setSingleKey(String[] singleKey) {
        this.singleKey = singleKey;
    }

    public boolean isNotEmpty(){
        return key!=null && key.length()>0;
    }

}
