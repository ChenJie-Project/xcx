package cog.support.springmvc.model;


/**
 * 请求返回实体类
 * 
 * @author 陈杰
 * @time 2016年9月12日 03:35:34
 * @version v0.1
 * 
 * Copyright DongHui(chenjie_java@aliyun.com)
 */
public class ResponseModel<T>{

	/**
	 * 请求处理状态
	 * */
	private int status;
	
	/**i
	 * 请求处理状态描述
	 * */
	private String describe;
	
	/**
	 * 请求返回时间
	 * */
	private long time;

	private String signature;
	
	/**
	 * 请求返回的具体数据
	 * */
	private T data = null;

	public ResponseModel(){}

	public ResponseModel(int status,String describe){
		this.status = status;
		this.describe = describe;
		this.time = System.currentTimeMillis();
	}

	public ResponseModel(int status,String describe,T data){
		this.status = status;
		this.describe = describe;
		this.time = System.currentTimeMillis();
		this.data = data;
	}

	public ResponseModel(T date){
		this.status = 0;
		this.describe = "请求成功";
		this.time = System.currentTimeMillis();
		this.data = date;
	}

	/**
	 * 创建新的请求实体类 
	 * 
	 * @param status 请求处理状态
	 * @param describe 请求处理状态描述
	 * @return ReponseModel
	 * */
	public static ResponseModel<Object>  returnModel(int status,String describe){
		return new ResponseModel<>(status, describe);
	}


	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * 创建新的请求实体类 
	 * 
	 * @param result 请求处理状态
	 * @return ReponseModel
	 * */
	public static ResponseModel<Object>  returnModel(boolean result){
		return new ResponseModel<>(result?0:-1, result?"操作成功":"操作失败");
	}
	
	/**
	 * 创建新的请求实体类 
	 * 
	 * @param status 请求处理状态
	 * @param describe 请求处理状态描述
	 * @param data 数据对象
	 * @return ReponseModel
	 * */
	public static <T> ResponseModel<T> returnModel(int status,String describe,T data){
		return new ResponseModel<>(status, describe,data);
	}
	
	/**
	 * 创建新的请求实体类 
	 * 
	 * @param  date 请求返回的结果集合
	 * @return ReponseModel
	 * */
	public static <T> ResponseModel<T> returnModel(T date){
		return new ResponseModel<>(date);
	}

    /**
     * 请求实体类返回Null，表示当前请求无数据返回
     *
     * @return ReponseModel
     * */
    public static ResponseModel<Object> returnNullModel(){
        return  new ResponseModel<>(-1,"无数据返回",new Object());
    }

	/**
	 * 请求实体成功返回，表示当前请求操作成功
	 *
	 * @return ReponseModel
     */
	public static ResponseModel<Object> returnActionSuccessModel(){
		return new ResponseModel<>(0,"操作成功");}

	/**
	 * 请求实体成功返回，表示当前请求操作成功
	 *
	 * @param successMessage 成功的消息
	 * @return ReponseModel
	 */
	public static ResponseModel<Object> returnActionSuccessModel(String successMessage){return new ResponseModel<>(0,successMessage);}

	/**
	 * 请求实体失败返回，表示当前请求操作失败
	 *
	 * @return ReponseModel
	 */
	public static ResponseModel<Object> returnActionErrorModel(){return new ResponseModel<>(-1,"操作失败");}

	/**
	 * 请求实体失败返回，表示当前请求操作失败
	 *
     * @param errorDesc 错误消息
	 * @return ReponseModel
	 */
	public static ResponseModel<Object> returnActionErrorModel(String errorDesc){return new ResponseModel<>(-1,errorDesc);}

	/**
	 * 通过影响行数来返回请求具体结果
	 *
	 * @return ReponseModel
	 */
	public static ResponseModel<Object> returnActionResult(Integer result){
		return result<=0 ? new ResponseModel<>(2,"操作失败") : new ResponseModel<>(0,"操作成功,影响"+result+"条记录!");
	}

	public static ResponseModel<Object> returnActionResult(Integer result,String successDesc,String errorDesc){
		return result<=0 ? new ResponseModel<>(2,errorDesc) : new ResponseModel<>(0,successDesc);
	}

	/**
	 * 通过影响行数来返回请求具体结果
	 *
	 * @return ReponseModel
	 */
	public static ResponseModel<Object> returnAsyncActionResult(){
		return new ResponseModel<>(0,"操作成功,后台正在异步更新!");
	}

	/**
	 * 通过影响行数来返回请求具体结果
	 *
	 * @return ReponseModel
	 */
	public static ResponseModel<Object> returnAsyncActionResult(Integer result){
		if(result>0)
			return new ResponseModel<>(0,"操作成功,后台正在异步更新!");
		return new ResponseModel<>(2,"操作失败");
	}

	/**
	 * 通过影响行数来返回请求具体结果
	 *
	 * @return ReponseModel
	 */
	public static ResponseModel<Object> returnActionResult(Integer result,Object da){
		ResponseModel<Object> model = new ResponseModel<>(result>=0?0:-1,result>=0?"操作成功":"操作失败");
        model.setData(da);
        return model;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public T getData() {
		return data;
	}

	public void setData(T date) {
		this.data = date;
	}

}
