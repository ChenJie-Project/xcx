package cog.support.common.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

/**
 * 公共模型父类，抽象通用属性和方法
 *
 * @author 陈杰
 * @since 2016年8月23日 11:53:15
 * @version v0.1
 *
 * Copyright ChenJie(chenjie@aliyun.com)
 */
public abstract class AbstractBaseModel implements Serializable {

    private Logger logger = LoggerFactory.getLogger(AbstractBaseModel.class);

    /**
     * 动态月份
     * */
    private int dynamicMonth = LocalDate.now().getMonthValue();

    public int getDynamicMonth() {
        return dynamicMonth;
    }

    public void setDynamicMonth(int dynamicMonth) {
        this.dynamicMonth = dynamicMonth;
    }

    /**
     * 绑定对象
     * */
    private Object attachedObject;

    public <E> E getAttachedObject() {
        return (E)attachedObject;
    }

    public void setAttachedObject(Object attachedObject) {
        this.attachedObject = attachedObject;
    }

    /**
     * 转换时间
     * @parm time 字符串时间 (yyyy-MM-dd HH:mm:ss)
     * */
    protected Date paraseDate(String time){
        try {
            return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(time);
        } catch (ParseException e) {
            logger.debug(e.getMessage(),e.getStackTrace());
        }
        return null;
    }

}
