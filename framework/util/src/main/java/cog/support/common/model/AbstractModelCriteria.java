package cog.support.common.model;

import java.util.LinkedList;
import java.util.List;

/**
 * 默认的MyBatis generator 的查询抽象
 *
 * @author 陈杰
 * @since 2016年9月19日 14:51:33
 * @version v0.1
 *
 * Copyright ChenJie(chenjie@aliyun.com)
 */
public class AbstractModelCriteria {

    public List<Criterion> criteria;

    public AbstractModelCriteria() {
        super();
        criteria = new LinkedList<>();
    }

    public boolean isValid() {
        return criteria.size() > 0;
    }

    public List<Criterion> getAllCriteria() {
        return criteria;
    }

    public List<Criterion> getCriteria() {
        return criteria;
    }

    public AbstractModelCriteria addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteria.add(new Criterion(condition));
        return this;
    }

    public AbstractModelCriteria addCriterion(String condition, Object value, String property) {
        if (value == null) {
            throw new RuntimeException("Value for " + property + " cannot be null");
        }
        criteria.add(new Criterion(condition, value));
        return this;
    }

    public AbstractModelCriteria addCriterion(String condition, Object value1, Object value2, String property) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + property + " cannot be null");
        }
        criteria.add(new Criterion(condition, value1, value2));
        return this;
    }
}
