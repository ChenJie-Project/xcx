package cog.support.common.model;

import java.time.LocalDate;

/**
 * Created by Administrator on 2016/9/12.
 */
public class DynamicMonth extends AbstractBaseModel {
    private int dynamicMonth = LocalDate.now().getMonthValue();

    public int getDynamicMonth() {
        return dynamicMonth;
    }

    public void setDynamicMonth(int dynamicMonth) {
        this.dynamicMonth = dynamicMonth;
    }
}
