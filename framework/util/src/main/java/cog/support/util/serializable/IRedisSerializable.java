package cog.support.util.serializable;

import java.util.Map;

/**
 * Redis序列化接口
 *
 * @author 陈杰
 * @since 2017年2月4日 22:07:49
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public interface IRedisSerializable {
    /**
     * 获取序列化唯一ID
     * */
    String getSerializableId();

    /**
     * 获取Redis的存储格式
     * */
    Map<String,String> getRedisMap();

    /**
     * 加载Redis数据
     * */
    void loadRedisData(Map<String,String> mapIn);

}
