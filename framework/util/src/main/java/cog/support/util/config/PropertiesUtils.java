package cog.support.util.config;

import cog.support.util.common.StringKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * 属性文件工具类
 *
 * @author 陈杰
 * @since 2017/11/24 13:58
 * Copyright ChenJie(wuzhiyun@aliyun.com)
 */
public class PropertiesUtils {
    private static ResourceLoader resourceLoader = new DefaultResourceLoader();

    private static Logger logger = LoggerFactory.getLogger(PropertiesUtils.class);

    private static Map<String,Properties> propertiesMap = new HashMap<>();

    private static Map<String,Object> config = new HashMap<>();

    /**
     * 加载配置文件
     *
     * @param filePathArray 文件的绝对路径，可以使用classpath:开头来代替项目根路径
     * */
    public static void loadProperty(String... filePathArray){
        for(String filePath : filePathArray) {
            try {
                if (StringKit.isNotEmpty(filePath)) {
                    //获取文件绝对路径
                    Resource resource = resourceLoader.getResource(filePath);
                    if(resource.exists()) {
                        String fileName = resource.getFilename();

                        //加载配置文件
                        Properties properties = new Properties();
                        properties.load(resource.getInputStream());

                        //加载配置文件数据
                        Set<Object> keys = properties.keySet();
                        for (Object key : keys) {
                            config.put(key.toString(), properties.getProperty(key.toString(), null));
                        }

                        //保存配置文件
                        propertiesMap.put(fileName, properties);
                    }else{
                        logger.info("没有找到路径{}的配置文件",filePath);
                    }
                }
            } catch (IOException e) {
                logger.info("读取配置文件出现异常,文件路径:{},异常信息:{}",filePath,e.getMessage(),e);
            }
        }
    }

    public static String getStr(String key) {
        return getStr(key,null);
    }
    public static String getStr(String key,String defaultValue) {
        Object val = config.get(key);
        if (val==null || "".equals(val)) {
            val = config.get(key);
        }
        return val == null ? defaultValue : val.toString();
    }

    public static long getLong(String key) {
        return getLong(key,0L);
    }
    public static long getLong(String key,long defaultValue) {
        String val = getStr(key);
        if ("".equals(val.trim())) {
            val = null;
        }
        return val==null?defaultValue:Long.parseLong(val);
    }

    public static boolean getBoolean(String key) {
        return getStr(key,"").equals("true");
    }

    public static int getInt(String key) {
        return getInt(key,0);
    }
    public static int getInt(String key,int defaultValue) {
        String val = getStr(key);
        if (val==null || "".equals(val.trim())) {
            val = null;
        }
        return val==null?defaultValue:Integer.parseInt(val);
    }

}
