package cog.support.util.excel.mapper.list;

import cog.support.util.excel.IExcelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * java实体类的转换格式
 *
 * @author 陈杰
 * @since 2016年12月1日 19:38:58
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com) 2016年9月12日 00:53:51
 */
public class JavaModelMapper<E> implements IExcelMapper<List<E>>{
    private Logger logger = LoggerFactory.getLogger(JavaModelMapper.class);

    @Override
    public void exportExcelFile(List<E> object, String excelFilePath) throws Exception {
        this.exportExcelFile(object,new FileOutputStream(excelFilePath));
    }

    @Override
    public void exportExcelFile(List<E> object, OutputStream os) throws Exception {
//        if(StringKit.isNotEmpty(excelFilePath) &&  object!=null && object.size()>0){
//            Field[] fields = object.get(0).getClass().getFields();
//            HSSFWorkbook wb = new HSSFWorkbook();
//            HSSFSheet sheet = wb.createSheet("java-model-export");
//            for(int i = 0;i<=object.size();i++){
//                E obj = object.get(i);
//                HSSFRow row = sheet.createRow(i);
//                for (int r = 0; r < fields.length;r++){
//                    HSSFCell cell = row.createCell(r);
//                    Field field = fields[r];
//                    Method getMethod = obj.getClass().getMethod(StringKit.getFieldGetterName(field.getName()));
//                    Object value = getMethod.invoke(obj);
//                    cell.setCellValue(value);
//                }
//            }
//            this.printFile(wb,os);
//        }else{
//            throw new Exception("文件路径，导出对象不能为空，且保证有数据");
//        }
    }

//    private void printFile(HSSFWorkbook wb,OutputStream fileOut){
//        try {
//            wb.write(fileOut);
//            fileOut.flush();
//        }catch (IOException ioe){
//            logger.error("到处文件出现异常，异常信息{}",ioe);
//        } finally {
//            fileOut.close();
//        }
//    }

    @Override
    public List<E> parseaToJavaObject(String excelFilePath) {
        return null;
    }

    @Override
    public List<E> parseaToJavaObject(InputStream is) {
        return null;
    }
}
