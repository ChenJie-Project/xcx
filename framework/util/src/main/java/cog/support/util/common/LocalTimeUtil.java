package cog.support.util.common;

import cog.support.util.common.StringKit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author 陈杰
 * @since 2017/8/20 0:46
 * <p>
 * Copyright ChenJie(wuzhiyun@aliyun.com)
 */
public class LocalTimeUtil {

    private static  DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * 获取时间差,秒
     *
     * @param beginTime 其实时间字符串
     * @param endTime 结束时间
     * */
    public static Integer getLocalDateTimesIntervalBySeconds(LocalDateTime beginTime, LocalDateTime endTime){
        if(beginTime == null || endTime == null) return 0;
        return Math.abs(Math.toIntExact(Duration.between(beginTime, endTime).toMillis() / 1000));
    }

    /**
     * 获取时间范围函数
     *
     * @param beginTime 其实时间字符串
     * @param endTime 结束时间
     * */
    public static LocalDateTime[] getLocalDateTime(String beginTime, String endTime){
        if(StringKit.isNotEmpty(beginTime) && StringKit.isNotEmpty(endTime)){
            LocalDateTime endDataTime = LocalDateTime.parse(endTime,dateTimeFormatter);
            LocalDateTime beginDateTime = LocalDateTime.parse(beginTime,dateTimeFormatter);
            if(endDataTime.isBefore(beginDateTime)){
                return null;
            }else if(beginDateTime.getMonthValue() != endDataTime.getMonthValue()){
                return null;
            }else if(beginDateTime.getMonthValue() - LocalDateTime.now().getMonthValue() >3){
                return null;
            }
            return new LocalDateTime[]{beginDateTime,endDataTime};
        }
        return null;
    }


    /**
     * 获取时间范围函数
     *
     * @param beginTime 其实时间字符串
     * @param endTime 结束时间
     * */
    public static LocalDateTime[] getLocalDateTimeBySms(String beginTime, String endTime) {
        if(StringKit.isNotEmpty(beginTime) && StringKit.isNotEmpty(endTime)){
            LocalDateTime endDataTime = LocalDateTime.parse(endTime,dateTimeFormatter);
            LocalDateTime beginDateTime = LocalDateTime.parse(beginTime,dateTimeFormatter);
            if(endDataTime.isBefore(beginDateTime)) {
                return null;
            }else if(beginDateTime.getMonthValue() - LocalDateTime.now().getMonthValue() >3){
                return null;
            }
            return new LocalDateTime[]{beginDateTime,endDataTime};
        }
        return null;
    }

    /**
     * 获取时间范围函数 查询所有
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public static LocalDateTime[] getLocalDateTimeAll(String beginTime, String endTime) {
        if(StringKit.isNotEmpty(beginTime) && StringKit.isNotEmpty(endTime)){
            LocalDateTime endDataTime = LocalDateTime.parse(endTime,dateTimeFormatter);
            LocalDateTime beginDateTime = LocalDateTime.parse(beginTime,dateTimeFormatter);
            if(endDataTime.isBefore(beginDateTime)){
                return null;
            }
            return new LocalDateTime[]{beginDateTime,endDataTime};
        }
        return null;
    }
}
