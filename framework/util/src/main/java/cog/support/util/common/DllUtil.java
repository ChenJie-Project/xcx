package cog.support.util.common;

import java.io.File;
import java.util.StringTokenizer;

/**
 * Created by WuZhiYun on 2017/4/26.
 */
public class DllUtil {
    public static void loadDll(String dllFilePath){
        try {
            // 获取到java.library.path  及系统变量中Path中的内容
            String libpath = System.getProperty("java.library.path");
            if (libpath == null || libpath.length() == 0) {
                throw new RuntimeException("java.library.path is null");
            }
            //javaBinPath   jdk的bin目录D:\Program Files\Java\jdk1.6.0_11\bin
            String javaBinPath = null;
            StringTokenizer st = new StringTokenizer(libpath,System.getProperty("path.separator"));
            if (st.hasMoreElements()) {
                javaBinPath = st.nextToken();
            } else {
                throw new RuntimeException("can not split library path:"
                        + libpath);
            }
            final File dllFile = new File(dllFilePath);
            if (dllFile.exists()) {
                // 动态加载dll
                System.load(dllFile.getPath());
                // 在虚拟机关闭的时候删除dll
                dllFile.deleteOnExit();
            }
        } catch (Throwable e) {
            throw new RuntimeException("load Convert.dll error!", e);
        }
    }
}
