package cog.support.util.serializable;


/**
 * Java对象序列化
 *
 * @author 陈杰
 * @since 2016年9月12日 00:53:51
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com) 2016年9月12日 00:53:51
 */
public interface CogSerializable<T> {

    /**
     * 序列化对象
     * */
    T serializableToObject(Object inObject);

    /**
     * 反序列化
     *
     * @param inObject
     * @param clazs
     * */
    <K> K serializableFromObject(T inObject,Class<?> clazs) throws Exception;
}
