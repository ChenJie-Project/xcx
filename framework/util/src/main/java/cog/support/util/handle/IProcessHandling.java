package cog.support.util.handle;

import cog.support.util.common.model.MapRecord;

/**
 * 过程处理器
 *
 * @author 陈杰
 * @since 2016年9月12日 00:53:51
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com) 2016年9月12日 00:53:51
 */
public interface IProcessHandling {
    /**
     * 处理当前过程
     * */
    Object handle(MapRecord<String,Object> parameters) throws Exception;

    /**
     * 处理下次过程
     * <p>此方法若重写，则skipHandle(String handleName)将失效</p>
     * */
    Object nextHandle(MapRecord<String,Object> parameters) throws Exception;


    /**
     * 设置下个过程的指向
     * */
    void linkNextHandle(IProcessHandling handling);
}
