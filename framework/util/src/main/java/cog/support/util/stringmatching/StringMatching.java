package cog.support.util.stringmatching;

import java.time.LocalDateTime;

/**
 * 内容模版匹配
 *
 * @author 陈杰
 * @since 2016年9月14日 03:45:44
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public interface StringMatching<T> {
	/**
	 * 查找匹配位置
	 * 
	 * @param sources 源字符串
	 * */
	StringMatchingModel<T> find(String sources);

    /**
     * 查找位置并且返回匹配字符串
     *
     * @param sources 源字符串
     * */
	StringMatchingModel<T> findReturn(String sources);

	/**
	 * 获取上次命中时间
	 * */
	LocalDateTime getLastHitTime();

	/**
	 * 获取并且清楚命中次数
	 * */
	int clearHitCount();

	/**
	 * 获取绑定的实体对象
	 * */
	T getObject();
}
