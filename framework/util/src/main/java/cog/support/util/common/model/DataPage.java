package cog.support.util.common.model;

import java.io.Serializable;
import java.util.List;

/**
 * 分页实体类，封装分页数据，页脚数据等
 * 
 * @author 陈杰
 * @time 2016年7月19日 18:36:08
 * @version v0.1
 * 
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public final class DataPage<T> implements Serializable {

	public static final int DEFAULT_PAGE_SIZE = 20;
	/**
	 * 默认的序列化版本 id.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 分页查询记录开始位置.
	 */
	private int begin;
	
	/**
	 * 分页查看下结束位置
	 */
	private int end;
	
	/**
	 * 每页显示记录条数
	 */
	private int pageSize = 20;
	
	/**
	 * 查询结果总记录数
	 */
	private int totalRecords;
	
	/**
	 * 当前页码.
	 */
	private int pageNo;
	
	/**
	 * 总共页数.
	 */
	private int totalPageCount;
	
	
	private List<T> listData;


	public void initPageInfo(int totalRecords){
		this.totalRecords = totalRecords;

		if(totalRecords >0 ){
			//计算总页数
			this.totalPageCount = (int) Math.ceil((this.totalRecords * 1.0d) / this.pageSize);

            //判断pageNo的有效性
            if(this.pageNo > this.totalPageCount ) this.pageNo = this.totalPageCount;
            if(this.pageNo<=0) this.pageNo = 1;

            //更加总页数求，记录开始和结束index
            this.begin = (this.pageNo-1) * this.pageSize;
            this.end = this.pageSize;
		}else{
			this.totalPageCount = 0;
		}
	}

	/**
	 * 构造函数，设置页数，自动计算数据范围.
	 * 
	 * @param pageNo 当前页面
	 */
	public DataPage(int pageNo) {
		this.pageNo = pageNo > 0 ? pageNo : 1;
	}
	
	/**
	 * 构造函数，设置页数,设置每页限时长度，自动计算数据范围.
	 * 
	 * @param pageNo 当前页面
	 */
	public DataPage(int pageNo,int pageSize){
		this.pageNo = pageNo > 0 ? pageNo : 1;
		this.pageSize = pageSize;
	}

	/**
	 * default
	 */
	public DataPage() {
		this.begin = 0;
		this.pageSize = 10;
	}

	public void setBegin(int begin) {
		this.begin = begin;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public void setTotalPageCount(int totalPageCount) {
		this.totalPageCount = totalPageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public List<T> getListData() {
		return listData;
	}

	public void setListData(List<T> listData) {
		this.listData = listData;
	}

	public int getBegin() {
		return begin;
	}

	public int getEnd() {
		return end;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public int getTotalPageCount() {
		return totalPageCount;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder("begin=").append(begin)
				.append(", end=").append(end).append(", pageSize=")
				.append(pageSize).append(", totalRecords=").append(totalRecords)
				.append(", pageNo=").append(pageNo).append(", pageCount=")
				.append(totalPageCount);
		return builder.toString();
	}
}