package cog.support.util.common.model;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 记录工具类，主要用于处理数据库返回的Map
 * 
 * <code>
 * MapRecord<String,Object> mapRecord = MapRecord.createRecord(map);
 * mapRecord.getString(key);
 * mapRecord.getInteger(key,defaultValue);
 * mapRecord.reLoadMap(map2);
 * mapRecord.getString(key);
 * </code>
 * 
 * @author 陈杰
 * @time 2016年4月14日 14:48:53
 * @version v0.1
 * 
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class MapRecord<K,V> extends HashMap<K,V> implements Map<K,V>{

	/**
	 * 初始化输入数据
	 * 
	 * @param map 输入数据
	 * @return MapRecord<K,V>
	 * */
	public MapRecord<K,V> reLoadMap(Map<K,V> map){
		super.clear();
		if(map!=null && map.size()>0)
			super.putAll(map);
		return this;
	}


	/**
	 * 添加Key，Value
	 * 
	 * @param key 键值
	 * @param value 默认值
	 * */
	public MapRecord<K,V> add(K key,V value){
		super.put(key, value);
		return this;
	}
	
	/**
	 * 获取String类型
	 * 
	 * @param key 键值
	 * 
	 * @return String value值
	 * */
	public String getString(K key){
		return this.getString(key,null);
	}
	
	/**
	 * 获取String类型
	 * 
	 * @param key 键值
	 * @param defaultValue 默认值
	 * @return String value值
	 * */
	public String getString(K key,String defaultValue){
		Object value = super.get(key);
		if(value!=null && StringUtils.isNotEmpty(value.toString())){
			return value.toString();
		}else{
			return defaultValue;
		}
	}
	public String removeString(K key){
		return this.removeString(key,null);
	}

	public String removeString(K key,String defaultValue){
		Object value = super.remove(key);
		if(value!=null && StringUtils.isNotEmpty(value.toString())){
			return value.toString();
		}else{
			return defaultValue;
		}
	}

	public long getLong(K key){
		return this.getLong(key,-1);
	}

	public long getLong(K key,long defaultValue){
		Object value = super.get(key);
		if(value!=null && StringUtils.isNotEmpty(value.toString())){
			if(value instanceof Long) return (long) value;
			return Long.parseLong(value.toString());
		}else{
			return defaultValue;
		}
	}


	public Long getLong(K key,Long defaultValue){
		Object value = super.get(key);
		if(value!=null && StringUtils.isNotEmpty(value.toString())){
			if(value instanceof Long) return (long) value;
			return Long.parseLong(value.toString());
		}else{
			return defaultValue;
		}
	}

	/**
	 * 获取int类型
	 * 
	 * @param key 键值
	 * @return int value值
	 * */
	public int getInteger(K key){
		return this.getInteger(key, -1);
	}
	/**
	 * 获取int类型
	 * 
	 * @param key 键值
	 * @param defaultValue 默认值
	 * @return int value值
	 * */
	public int getInteger(K key,int defaultValue){
		Object value = super.get(key);
		if(value!=null && StringUtils.isNotEmpty(value.toString())){
			if(value instanceof Integer) return (int) value;
			return Integer.parseInt(value.toString());
		}else{
			return defaultValue;
		}
	}


	/**
	 * 获取int类型
	 *
	 * @param key 键值
	 * @param defaultValue 默认值
	 * @return int value值
	 * */
	public Integer getInteger(K key,Integer defaultValue){
		Object value = super.get(key);
		if(value!=null && StringUtils.isNotEmpty(value.toString())){
			if(value instanceof Integer) return (int) value;
			return Integer.parseInt(value.toString());
		}else{
			return defaultValue;
		}
	}

	public int removeInteger(K key){
		return this.removeInteger(key,-1);
	}
	public int removeInteger(K key,int defaultValue){
		Object value = super.remove(key);
		if(value!=null && StringUtils.isNotEmpty(value.toString())){
			if(value instanceof Integer) return (int) value;
			return Integer.parseInt(value.toString());
		}else{
			return defaultValue;
		}
	}
	/**
	 * 获取boolean类型
	 * 
	 * @param key 键值
	 * @return boolean value值
	 * */
	public boolean getBoolean(K key){
		return this.getBoolean(key,false);
	}

	/**
	 * 获取boolean类型
	 * 
	 * @param key 键值
	 * @param defaultValue 默认值
	 * @return boolean value值
	 * */
	public boolean getBoolean(K key,boolean defaultValue){
		Object value = super.get(key);
		if(value!=null && StringUtils.isNotEmpty(value.toString())){
			return value.toString().equals("true");
		}else{
			return defaultValue;
		}
	}

	/**
	 * 获取Double类型
	 *
	 * @param key 键值
	 * @param defaultValue 默认值
	 * @return boolean value值
	 * */
	public double getDouble(K key,double defaultValue){
		Object value = super.get(key);
		if(value!=null && StringUtils.isNoneEmpty(value.toString())){
			return Double.parseDouble(value.toString());
		}else{
			return defaultValue;
		}
	}

    /**
     * 获取Java对象
     *
     * @param key 键值
     * @return boolean value值
     * */
	public <T> T getJavaObject(K key){
        Object value = super.get(key);
        if(value!=null){
            return (T)value;
        }
        return null;
    }
}
