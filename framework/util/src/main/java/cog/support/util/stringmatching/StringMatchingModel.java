package cog.support.util.stringmatching;

/**
 * 内容模版数据实体类
 *
 * @author 陈杰
 * @since 2016年9月14日 03:45:44
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class StringMatchingModel<T> {
    private String matchingStrs;

    private int patternCount;

    private String type;

    private String cacheId;

    private T matchingMode;

    public StringMatchingModel(String matchingStrs,T matchingMode){
        this.matchingStrs = matchingStrs;
        this.matchingMode = matchingMode;
        this.type = "find";
        this.patternCount = 0;
    }

    public StringMatchingModel(T matchingMode,String cacheId){
        this.matchingStrs = "";
        this.matchingMode = matchingMode;
        this.type = "cache";
        this.cacheId = cacheId;
        this.patternCount = 0;
    }

    public StringMatchingModel(T matchingMode){
        this.matchingMode = matchingMode;
        this.patternCount = 0;
        this.type = "find";
        this.matchingStrs = "";
        this.cacheId = "";
    }

    public String getCacheId() {
        return cacheId;
    }

    public void setCacheId(String cacheId) {
        this.cacheId = cacheId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPatternCount() {
        return patternCount;
    }

    public void setPatternCount(int patternCount) {
        this.patternCount = patternCount;
    }


    public T getMatchingMode() {
        return matchingMode;
    }

    public void setMatchingMode(T matchingMode) {
        this.matchingMode = matchingMode;
    }

    public String getMatchingStrs() {
        return matchingStrs;
    }

    public void setMatchingStrs(String matchingStrs) {
        this.matchingStrs = matchingStrs;
    }
}
