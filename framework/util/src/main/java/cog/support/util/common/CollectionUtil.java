package cog.support.util.common;

import org.apache.commons.collections.ArrayStack;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * String工具类
 *
 * @author 陈杰
 * @time 2016年4月14日 14:48:53
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class CollectionUtil {

    /**
     * 返回集合第一条元素，如果集合不为空，并且大小大于0
     *
     * @param  inList
     * @return T
     * */
    public static <T> T returnFirstIfNotNull(List<T> inList){
        return CollectionUtil.returnIndexIfNotNull(inList,0);
    }

    /**
     * 返回集合第一条元素，如果集合不为空，并且大小大于0
     *
     * @param  inList
     * @return T
     * */
    public static <T> T returnIndexIfNotNull(List<T> inList,int index){
        if(index<0) index = 0;
        return inList != null && inList.size() > index ? inList.get(index) : null;
    }


    public static String join(List<?> listIn,String joinStr){
        StringBuilder returnString = new StringBuilder();
        for(int i =0;i<listIn.size();i++){
            returnString.append(String.valueOf(listIn.get(i)));
            if(i<listIn.size()-1)
                returnString.append(joinStr);
        }
        return returnString.toString();
    }

    public static <T> String join(T[] listIn,String joinStr){
        StringBuilder returnString = new StringBuilder();
        for(int i =0;i<listIn.length;i++){
            returnString.append(String.valueOf(listIn[i]));
            if(i<listIn.length-1)
                returnString.append(joinStr);
        }
        return returnString.toString();
    }

    /**
     * 拆分集合
     *
     * @param <T> 泛型对象
     * @param resList 需要拆分的集合
     * @param subListLength 每个子集合的元素个数
     * @return 返回拆分后的各个集合组成的列表
     * 代码里面用到了guava和common的结合工具类
     **/
    public static <T> List<List<T>> split(List<T> resList, int subListLength) {
        List<List<T>> returnList = new ArrayList<>();
        if (CollectionUtils.isEmpty(resList) || subListLength <= 0) {
            returnList.add(resList);
            return returnList;
        }
        int size = resList.size();
        if (size <= subListLength) {
            // 数据量不足 subListLength 指定的大小
            returnList.add(resList);
        } else {
            int pre = size / subListLength;
            int last = size % subListLength;
            // 前面pre个集合，每个大小都是 subListLength 个元素
            for (int i = 0; i < pre; i++) {
                List<T> itemList = new LinkedList<>();
                for (int j = 0; j < subListLength; j++) {
                    itemList.add(resList.get(i * subListLength + j));
                }
                returnList.add(itemList);
            }
            // last的进行处理
            if (last > 0) {
                List<T> itemList = new LinkedList<>();
                for (int i = 0; i < last; i++) {
                    itemList.add(resList.get(pre * subListLength + i));
                }
                returnList.add(itemList);
            }
        }
        return returnList;
    }


    /**
     * 数组转换LIST
     * */
    public static <T> List<T> parseArrayToList(T[] arrary){
        List<T> returnList = new ArrayList<T>();
        if(arrary!=null )
            for(T t : arrary) returnList.add(t);
        return returnList;
    }


    public static <M> List<M> createList(M object){
        List<M> returnList = new ArrayList<>();
        if(object!=null) returnList.add(object);
        return returnList;
    }

    /**
     * 返回集合toString
     * */
    public static String parseListToJsonString(List<?> inList,String cutStr,String quoteStr,int limit){
        if(inList!=null && inList.size()>0){
            String cs = StringKit.isNotEmpty(cutStr) ? cutStr : "";
            String qs = StringKit.isNotEmpty(quoteStr) ? quoteStr : "";
            int length = inList.size()>limit?limit:inList.size();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[");
            for(int i = 0;i<length;i++){
                Object obj = inList.get(i);
                stringBuilder.append(qs).append(obj.toString()).append(qs);
                if(i < length) stringBuilder.append(cs);
            }
            return stringBuilder.toString();
        }
        return "[]";
    }
}
