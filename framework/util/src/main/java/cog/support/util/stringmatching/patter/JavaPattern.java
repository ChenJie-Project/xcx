package cog.support.util.stringmatching.patter;

import cog.support.util.stringmatching.StringMatching;
import cog.support.util.stringmatching.StringMatchingModel;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * java 正则的实现方式
 *
 * @author 陈杰
 * @since 2016年9月14日 03:45:44
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class JavaPattern<T> implements StringMatching<T>{
	private Pattern patternReplace = Pattern.compile("(\\\\|\\^|\\$|\\*|\\+|\\?|\\{|\\}|\\?|\\.|\\(|\\)|\\||\\[|\\]){1}");

	private T object;
	
	private Pattern pattern;

	private String patternStrs;

	public JavaPattern(boolean noRepacle,String patternStr,T t){
		this.object = t;
		if(noRepacle) {
			String patternStrWithOutRulers = this.removeRulers(patternStr);
			String patternStrs = Pattern.compile("%+").matcher(patternStrWithOutRulers).replaceAll("(.*)");
			this.pattern = Pattern.compile(patternStrs);
			this.patternStrs = patternStrs;
		}else {
			this.pattern = Pattern.compile(patternStr);
			this.patternStrs = patternStr;
		}
	}

	public JavaPattern(String patternStr,T t){
		this.object = t;
	    String patternStrWithOutRulers = this.removeRulers(patternStr);
	    String patternStrs = Pattern.compile("%+").matcher(patternStrWithOutRulers).replaceAll("(.*)");
	    this.pattern = Pattern.compile(patternStrs);
		this.patternStrs = patternStr;
	}

	public JavaPattern(String replaceRuler,String replaceStr,String patternStr,T t){
		this.object = t;
		String patternStrs = patternStr.replaceAll(replaceRuler,replaceStr);
		this.pattern = Pattern.compile("^"+patternStrs+"$");
		this.patternStrs = "^"+patternStrs+"$";
	}

	@Override
	public StringMatchingModel<T> find(String sources) {
		return pattern.matcher(sources).find()?new StringMatchingModel<>(this.object):null;
	}

	@Override
	public StringMatchingModel<T> findReturn(String sources) {
		Matcher matcher = pattern.matcher(sources);
		if(matcher.find()){//只需要匹配一次就可以
            int groupCount = matcher.groupCount();
            StringBuffer matchingStrs = new StringBuffer();
            for(int i = 1;i<=groupCount;i++){
            	String maStr = matcher.group(i);
            	if(!this.patternStrs.contains(maStr)){ // 如果命中的字符，在匹配规则出现，那么说明不是规则命中
            		matchingStrs.append(maStr);
					if(i<groupCount) matchingStrs.append("_");
				}
            }
			return new StringMatchingModel<>(matchingStrs.toString(),this.object);
		}
		return null;
	}

	@Override
	public LocalDateTime getLastHitTime() {
		return null;
	}

	@Override
	public int clearHitCount() {
		return 0;
	}

	@Override
	public T getObject() {
		return object;
	}

	private String removeRulers(String patternStr){
		Matcher matcher = patternReplace.matcher(patternStr);
	    StringBuffer sb = new StringBuffer(); 
	    while(matcher.find()) {
	        matcher.appendReplacement(sb,"."); 
	    } 
	    matcher.appendTail(sb);
	    sb.insert(0,"^").append("$");
	    return sb.toString();
	}
}
