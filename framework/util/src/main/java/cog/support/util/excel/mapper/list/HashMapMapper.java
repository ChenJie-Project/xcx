package cog.support.util.excel.mapper.list;


import cog.support.util.excel.IExcelMapper;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

/**
 * java HashMap方式的的转换格式
 *
 * @author 陈杰
 * @since 2016年12月1日 19:38:58
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com) 2016年9月12日 00:53:51
 */
public class HashMapMapper<E> implements IExcelMapper<Map<String,E>> {

    @Override
    public void exportExcelFile(Map<String, E> object, String excelFilePath) {

    }

    @Override
    public void exportExcelFile(Map<String, E> object, OutputStream os) {

    }

    @Override
    public Map<String, E> parseaToJavaObject(String excelFilePath) {
        return null;
    }

    @Override
    public Map<String, E> parseaToJavaObject(InputStream is) {
        return null;
    }
}
