package cog.support.util.excel;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * excel表格导入接口
 *
 * @author 陈杰
 * @since 2016年12月1日 19:38:58
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com) 2016年9月12日 00:53:51
 */
public interface IExcelMapper<T>{
    /**
     * 导出对象至excel文件
     *
     * @param object 导入的对象
     * @param excelFilePath 导出文件路径
     * */
    void exportExcelFile(T object,String excelFilePath) throws Exception;

    /**
     * 导出对象至excel文件
     *
     * @param object 导入的对象
     * @param os 输出流
     * */
    void exportExcelFile(T object, OutputStream os) throws Exception;

    /**
     * 导入excel表格
     *
     * @param excelFilePath 表格文件路径
     * @return T 导出的java对象
     * */
    T parseaToJavaObject(String excelFilePath);

    /**
     * 导入excel表格
     *
     * @param is 表格文件输入流
     * @return T 导出的java对象
     * */
    T parseaToJavaObject(InputStream is);

}
