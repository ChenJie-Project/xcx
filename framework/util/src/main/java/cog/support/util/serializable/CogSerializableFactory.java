package cog.support.util.serializable;

import cog.support.util.serializable.hashmap.CogHashMapSerializable;

import java.util.Map;

/**
 * Java对象序列化,创建工厂
 *
 * @author 陈杰
 * @since 2016年11月18日 15:28:14
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com) 2016年9月12日 00:53:51
 */
public class CogSerializableFactory {

    /**
     * 创建HashMap方式的序列化
     * */
    public static CogSerializable<Map<String,String>> getHashMapSerializable(){
        return new CogHashMapSerializable<>(String.class);
    }

    public static <T>  CogSerializable<Map<String,T>> getHashMapSerializable(Class<?> inClass){
        return new CogHashMapSerializable<>(inClass);
    }

}
