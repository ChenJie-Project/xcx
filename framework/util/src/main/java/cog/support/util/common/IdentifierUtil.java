package cog.support.util.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Id生成工具类
 *
 * @author 陈杰
 * @time 2016年4月14日 14:48:53
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class IdentifierUtil {

    /**
     * cmpp msgid 生成的日期格式化
     * */
    private final static SimpleDateFormat msgIdDateFormat =new SimpleDateFormat("MMddHHmmss");
    /**
     * cmpp msgid 的序列号
     * */
    private static int serialNumber = 0;

    /**
     * 获取cmpp接口的msgid
     * */
    public static String generateCmppMsgId(){
        return IdentifierUtil.generateCmppMsgId(1,System.currentTimeMillis());
    }

    /**
     * 获取cmpp接口的msgid
     *
     * @param index 长短信下标，从1开始
     * */
    public static String generateCmppMsgId(int index){
        return IdentifierUtil.generateCmppMsgId(index,System.currentTimeMillis());
    }

    /**
     * 获取cmpp接口的msgid
     *
     * @param index 长短信下标，从1开始
     * @param uuixTime uuix时间
     * */
    public static String generateCmppMsgId(int index,long uuixTime){
        if(serialNumber>99990) serialNumber = 0;
        return StringKit.stringSupplement(String.valueOf(++serialNumber),5) + "-"+index+"888888-" + msgIdDateFormat.format(new Date(uuixTime));
    }
}
