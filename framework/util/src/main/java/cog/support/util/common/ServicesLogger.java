package cog.support.util.common;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 服务日志处理类
 *
 * @author 陈杰
 * @since 2016年9月20日 14:22:33
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class ServicesLogger {
    /**
     * LocalDateTime的格式化
     * */
    private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static Logger logger = LoggerFactory.getLogger(ServicesLogger.class);
    private static Logger loggerSend = LoggerFactory.getLogger("SEND-ASYNC-LOGGER");


    public static final String MODEL_INSERT_CACHE = "InsertCache";

    /**
     * 记录短信日志信息
     *
     * @param modelName 模块名称
     * @param data 数据对象
     * */
    public static void recordLog(String modelName,String message,Object data){
        String jsonString = data!=null ? JSON.toJSONString(data) : "(empty)";
        loggerSend.info("<{}> {},Data:{}",modelName,message,jsonString);
    }



    /**
     * 记录短信日志信息
     *
     * @param modelName 模块名称
     * @param data 数据对象
     * */
    public static void recordLog(String modelName,String message,Object ...data){
        loggerSend.info("<"+modelName+"> " + message,data);
    }

    /**
     * 记录异常的短信日志信息
     *
     * @param modelName 模块名称
     * @param data 数据对象
     * */
    public static void recordExceptionLog(Exception e,String modelName,Object data){
        String jsonString = data!=null ? JSON.toJSONString(data) : "(empty)";
        recordExceptionLog(e,modelName,jsonString);
    }

    /**
     * 记录异常的短信日志信息
     *
     * @param modelName 模块名称
     * @param dataString 数据对象（json格式）
     * */
    public static void recordExceptionLog(Exception e,String modelName,String dataString){
        String logString = "\r\n[Logging]---------------------------------------------------------------------------------------------------"
                         + "\r\n[{}] [" + LocalDateTime.now().format(dateTimeFormatter) + "] [" + Thread.currentThread().getName() + "]"
                         + "\r\n[Data]------------------------------------------------------------------------------------------------------"
                         + "\r\n{}"
                         + "\r\n[Exception]-------------------------------------------------------------------------------------------------\r\n{}\r\n\r\n";
        logger.info(logString,modelName,dataString,e);
    }

}
