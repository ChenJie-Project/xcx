package cog.support.util.common.model;

import java.util.Map;
import java.util.TreeMap;

/**
 * 树模型数据实体类
 *
 * @author 陈杰
 * @time 2016年7月19日 18:36:08
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com)
 */
public class TreeModel<T> {
    private T treeObject;

    private Map<String,TreeModel<T>> childList;

    public TreeModel(){
        this.childList = new TreeMap<String,TreeModel<T>>();
    }

    public TreeModel(T treeObject){
        this.treeObject = treeObject;
    }

    public T getTreeObject() {
        return treeObject;
    }

    public void setTreeObject(T treeObject) {
        this.treeObject = treeObject;
    }

    public Map<String,TreeModel<T>> getChildList() {
        return childList;
    }

    public void setChildList(Map<String,TreeModel<T>> childList) {
        this.childList = childList;
    }

    public void addTreeModel(String treeKey,TreeModel<T> treeObject){
        this.childList.put(treeKey,treeObject);
    }
}
