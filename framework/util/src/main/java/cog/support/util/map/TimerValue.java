package cog.support.util.map;

public class TimerValue<K,V> {
    private V value;
    private K key;

    private long timeMillis;

    public TimerValue(K k,V v) {
        this.key = k;
        this.value = v;
        this.timeMillis = System.currentTimeMillis();
    }

    public V getValue() {
        return value;
    }

    public K getKey() {
        return key;
    }

    public long getTimeMillis() {
        return timeMillis;
    }
}