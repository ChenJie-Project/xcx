package com.wzy.xcx.application.business;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XcxSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(XcxSystemApplication.class, args);
    }
}
