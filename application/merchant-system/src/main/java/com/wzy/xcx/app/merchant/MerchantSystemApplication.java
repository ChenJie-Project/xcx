package com.wzy.xcx.app.merchant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MerchantSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(MerchantSystemApplication.class, args);
    }
}
