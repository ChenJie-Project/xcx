package com.wzy.xcx.services.orders.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersServicesApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrdersServicesApiApplication.class, args);
    }
}
