package com.wzy.xcx.services.merchant.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MerchantServicesApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MerchantServicesApiApplication.class, args);
    }
}
